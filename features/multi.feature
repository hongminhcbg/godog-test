Feature: Multi two numbers
    Multi two integer numbers

Scenario: Add 3 and 7 will have 21
    When Multi-I have 3 and 7
    Then Multi-I multi them
    And Multi-I have 21

Scenario: Add 3 and 70 will have 210
    When Multi-I have 3 and 70
    Then Multi-I multi them
    And Multi-I have 210
