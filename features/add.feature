Feature: Add two number

    Add two integer number

Scenario: Add 3 and 7 will have 10
    When I have 3 and 7
    Then I add them
    And I have 10

Scenario: Add 3 and 70 will have 10
    When I have 3 and 70
    Then I add them
    And I have 73
