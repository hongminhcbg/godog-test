package main

import (
	"fmt"
	"godog/math"
)

var resultMulti int

type Multi struct{}

func (m *Multi) multiIHave(arg1 int) error {
	if arg1 != resultMulti {
		return fmt.Errorf("Have = %d, want:%d", resultMulti, arg1)
	}

	return nil
}

func (m *Multi) multiIHaveAnd(arg1, arg2 int) error {
	resultMulti = math.Multi(arg1, arg2)

	return nil
}

func (m *Multi) multiIMultiThem() error {
	return nil
}
