package main

import (
	"flag"
	"fmt"
	"godog/math"
	"os"
	"testing"

	"github.com/cucumber/godog"
	"github.com/cucumber/godog/colors"
)

var opts = godog.Options{Output: colors.Colored(os.Stdout)}

func init() {
	godog.BindCommandLineFlags("godog.", &opts)
}

var result int

func iAddThem() error {
	return nil
}

func iHavaAnd(arg1, arg2 int) error {
	result = math.Add(arg1, arg2)

	if result < arg1 || result < arg2 {
		return fmt.Errorf("result = %d, overfollow", result)
	}

	return nil
}

func iHave(arg1 int) error {
	if arg1 != result {
		return fmt.Errorf("Have = %d, want:%d", result, arg1)
	}

	return nil
}

func InitializeScenario(ctx *godog.ScenarioContext) {
	// add two numbers
	ctx.Step(`^I add them$`, iAddThem)
	ctx.Step(`^I have (\d+) and (\d+)$`, iHavaAnd)
	ctx.Step(`^I have (\d+)$`, iHave)

	m := &Multi{}
	ctx.Step(`^Multi-I have (\d+)$`, m.multiIHave)
	ctx.Step(`^Multi-I have (\d+) and (\d+)$`, m.multiIHaveAnd)
	ctx.Step(`^Multi-I multi them$`, m.multiIMultiThem)
}

func InitializeTestSuite(ctx *godog.TestSuiteContext) {
	ctx.BeforeSuite(func() {})
}

func TestMain(m *testing.M) {
	flag.Parse()
	opts.Paths = flag.Args()

	status := godog.TestSuite{
		Name:                 "godogs",
		TestSuiteInitializer: InitializeTestSuite,
		ScenarioInitializer:  InitializeScenario,
		Options:              &opts,
	}.Run()

	os.Exit(status)
}
